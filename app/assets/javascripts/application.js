// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require_tree .

function append_time(e, text){
  e.append("<div><span class='label label-success'><i class='icon icon-time'></i> "+text+"</span></div>")
}

jQuery(document).ready(function($) {

  $('#description_info').hide()

  $('#description_btn').click(function(){
    $.getJSON("/events/"+location.pathname.split("/")[2]+".json", function(data) {
      $('#description_btn').hide()
      $('#description_info').append(data["description"]).show()

      var start = new Date(data["start"])
      var end = new Date(data["end"])
      var days = Math.round((end-start)/86400000)
      if (days < 1){
        append_time($('#description_info'), "impreza potrwa 1 dzień")
      }
      else {
        append_time($('#description_info'), "impreza potrwa "+days+" dni!")
      }
    })
  })  
})