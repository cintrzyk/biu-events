jQuery(document).ready(function($) {
  $('#gallery_content').hide()

  var posters = [];
  var index = 0;

  $('#get_gallery').click(function() {
    $.getJSON("/posters.json", function(data) {
      $('#get_gallery').hide()
      $('#gallery_content').show()
      $.each(data, function(key, val) {
        posters.push("<img class='img-polaroid' src='"+val["poster"]["url"]+"'>");
      })
      set_image()
    })
  })

  $('#next_image').click(function(){
    if (index_last())
      index = 0
    else
      index += 1
    set_image()
  })
  
  $('#prev_image').click(function(){
    if (index_first())
      index = posters.length-1
    else
      index -= 1
    set_image()
  })

  function set_image(){
    $('#image_content').html(posters[index])
  }

  function index_last(){
    if (posters.length-1 == index)
      return true
    return false
  }

  function index_first(){
    if (index == 0)
      return true
    return false
  }

})
