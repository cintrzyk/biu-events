set_validation_color = (element, flag) ->
  if flag
    element.removeClass('error')
    element.addClass('success')
  else
    element.addClass('error')
    element.removeClass('success')

validate_name = ->
  name = $('input[id=event_name]')
  control_group = name.parent().parent('.control-group')
  regexp = /^[a-zA-Zśćżóźłę]{3,}[0-9 \s a-zA-Zśćżóźłę]*$/
  if regexp.test name.val()
    set_validation_color control_group, true
    true
  else
    set_validation_color control_group, false
    false

validate_description = ->
  description = $('textarea[id=event_description]')
  control_group = description.parent().parent('.control-group')
  if description.val().length < 10
    set_validation_color control_group, false
    false
  else
    set_validation_color control_group, true
    true

jQuery ->
  $('#new_event').tooltip_hints()
  $('textarea[id=event_description]').change ->
    validate_description()
  $('input[id=event_name]').change ->
    validate_name()
  $('#new_event').submit ->
    validate_name() and validate_description()