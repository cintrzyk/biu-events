(function( $ ) {
  $.fn.tooltip_hints = function(options) {
    
    var opt = $.extend({
      'animation' : true,
      'html' : false,
      'selector' : false,
      'placement' : 'right',
      'trigger' : 'focus',
      'delay' : 0
    }, options)

    this.find('.help-block, .help-inline').hide()
  
    return this.find('input, textarea').each(function() {
      var title = $(this).next('.help-block, .help-inline').text()
      $(this).tooltip({
        'title' : title,
        'placement' : opt.placement, 
        'trigger' : opt.trigger, 
        'animation' : opt.animation,
        'html' : opt.html,
        'selector' : opt.selector,
        'delay' : opt.delay
      })
    })
  }
})( jQuery )