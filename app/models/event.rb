class Event < ActiveRecord::Base
  attr_accessible :description, :end, :name, :poster, :start

  mount_uploader :poster, PosterUploader
end
